#include "cmystring.h"
#include <string.h>


CString::CString() :m_pChar(nullptr) { 

} 

//Parametrise Constructor having " char*" as a parameter
CString::CString(const char* str) :m_pChar(nullptr) {  
	setData(str);
}
	
//Parametrise Constructor having " CString" as a parameter(Copy constructor)
CString::CString(const CString& rhs) { 
	*this = rhs;
}

//  Overload " << " operator to print object data
ostream& operator <<(ostream& o, const CString& obj) { 
	if (obj.m_pChar != nullptr) {
		o << obj.m_pChar;
	}
	return o;
}
	
// Overload " = " operator to store char* into object 
CString& CString::operator =(const char* str) { 
	setData(str);
	return *this;
}
	
// Overload " = " operator to assign one object to another object
CString& CString::operator =(const CString& rhs) {	
	if (this != &rhs) {
		*this = rhs.m_pChar;
	}
	return *this;
}

//Overload " + " operator to add 2 objects Ex. s1 + s2
CString CString::operator +(const CString& rhs) const { 
	return concatData(m_pChar, rhs.m_pChar);
}

//Overload " + " operator to add object and char* Ex. s1 + "Hello"
CString CString::operator +(const char* str) const { 
	return concatData(m_pChar, str);
}
	
CString::~CString() {
	if (m_pChar != nullptr) {
		delete[] m_pChar;
		m_pChar = nullptr;
	}
}
	
void CString::setData(const char* str) {
	if (m_pChar != nullptr) {
		delete[] m_pChar;
		m_pChar = nullptr;
	}
	if (str != nullptr)
	{
		m_pChar = new char[strlen(str) + 1];
		strcpy(m_pChar, str);
	}
}

CString operator +(const char* lhs, const CString& rhs) {
	return CString::concatData(lhs, rhs.m_pChar);
}

CString CString::concatData(const char* str1, const char* str2) 
{
	char* result = nullptr;
	if (str2 != nullptr) {
		if (str1 != nullptr) {
			result = new char[strlen(str1) + strlen(str2) + 1];
			strcpy(result, str1);
			strcat(result, str2);
		}
		else {
			result = new char[strlen(str2) + 1];
			strcpy(result, str2);
		}
	}
	else {
		if (str1 != nullptr) {
			result = new char[strlen(str1) + 1];
			strcpy(result, str1);
		}
		else {
			result = nullptr;
		}
	}
	CString temp(result);
	if (result != nullptr) {
		delete[] result;
		result = nullptr;
	}
	return temp;
}
