#pragma once
#include <iostream>

using namespace std;

class CString {
public:
	CString();
	CString(const char* str);
	CString(const CString& rhs);

	friend ostream& operator <<(ostream& o, const CString& obj);
	
	CString& operator =(const char* str);
	CString& operator =(const CString& rhs);

	CString operator +(const CString& rhs) const;
	CString operator +(const char* str) const;
	friend CString operator +(const char* lhs, const CString& rhs);

	~CString();
private:
	char* m_pChar;
	void setData(const char* str);
	static CString concatData(const char* str1, const char* str2) ;
};
